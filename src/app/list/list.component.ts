import { Component, OnInit } from '@angular/core';
import {Todo} from "../../models/todo.model";
import {TodoService} from "../todo.service";
import {MatDialog} from "@angular/material/dialog";
import {EditFormComponent} from "../edit-form/edit-form.component";
import {PageEvent} from "@angular/material/paginator";

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit{

  todo : Todo;
  done : boolean = false;
  notDone : boolean = true;
  listLength: number = this.todoService.getListLength();
  entryPerPage: number = 5;
  currentPage: number = 1;
  c: number = 1;
  listArray: Todo[] = [];
  lastUpdated: Todo;
  pageEvent = PageEvent;

  checkList: Todo [] = [];


  constructor(private todoService: TodoService,
              private dialog: MatDialog) {}


  ngOnInit() {
    this.addTodo();
  }

  addTodo(){
   this.listArray = this.todoService.getALlTodos();
  }

  onComplete(id: number){
    this.todoService.setCompleted(id);
  }

  onDelete(id: number){
    this.todoService.deleteItem(id);
  }

  onDone(){
    this.done = true;
    this.notDone = false;
  }

  onNotDone(){
    this.notDone = true;
    this.done = false;
  }

  onBoth(){
    this.done = true;
    this.notDone = true;
  }

  onEdit(todo: Todo){
    const index = this.listArray.indexOf(todo);

    let dialogRef = this.dialog.open(EditFormComponent, {
      width: '100%',
      data: todo,
    });

    dialogRef.afterClosed().subscribe((result) => {
      if(result){
        this.todoService.updateTodo(index, result);
        this.lastUpdated = this.todoService.getLastUpdated();
        //console.log('from list ', this.lastUpdated);
      }
    });
  }

  allowDrop($event): void{
    this.todoService.allowDrop($event);
  }

  // drag(e){
  //   e.dataTransfer.setData("text", e.target.id);
  // }
  //
  // drop(e){
  //   e.preventDefault();
  //   let data = e.dataTransfer.getData("text");
  //   e.target.appendChild(document.getElementById(data));
  // }

  onDragStart(index): void{
    this.todoService.onDragStart(index);
  }

  onDropOpen($event, index): void{
    this.todoService.onDropOpen($event, index);
  }

  onDropDone($event, index): void{
    this.todoService.onDropDone($event, index);
  }



  onChecked(todo: Todo){
    let checked: boolean = false;
    let index: number;
    for(let i = 0; i < this.checkList.length; i++){
      if(this.checkList[i] === todo){
        checked = true;
        index = i;
        break;
      }
      else {
        checked = false;
      }
    }
    if(checked){
      this.checkList.splice(index,1);
      return;
    }else{
      this.checkList.push(todo);
      console.log(this.checkList);
    }
  }

  deleteChecked(){
    for(let i = 0; i < this.checkList.length; i++){
      this.onDelete(this.checkList[i].id);
    }
    this.checkList = [];
  }

  completedCheckList(){
    for(let i = 0; i < this.checkList.length; i++){
      this.onComplete(this.checkList[i].id);
    }
    this.checkList = [];
  }






























}

