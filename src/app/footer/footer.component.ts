import { Component, OnInit } from '@angular/core';
import {TodoService} from "../todo.service";
import {Todo} from "../../models/todo.model";

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  todosLength: number;

  constructor(private todoService: TodoService) { }

  ngOnInit(): void {
  }

  getNotCompletedLength(): number{
    return this.todoService.getAllNotCompleted().length;
  }

  getCompletedLength(): number{
    return this.todoService.getAllCompleted().length;
  }

  getNextTodo(): Todo{
    return this.todoService.getNextTodo();
  }

  getLastUpdated(): Todo{
    if(this.todoService.getLastUpdated())
      return this.todoService.getLastUpdated();
    else
      return null;
  }



}
