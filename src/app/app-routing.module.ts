import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {AppComponent} from "./app.component";
import {FormComponent} from "./form/form.component";
import {ListComponent} from "./list/list.component";
import {HomeComponent} from "./home/home.component";
import {EditFormComponent} from "./edit-form/edit-form.component";

const routes: Routes = [
  {path: '', redirectTo:'/home', pathMatch: 'full'},
  {path: 'home', component: HomeComponent},
  {path: 'form', component:FormComponent },
  {path: 'list', component:ListComponent},
  {path: 'edit-form', component:EditFormComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
