import { Injectable } from '@angular/core';
import {Todo} from "../models/todo.model";

@Injectable({
  providedIn: 'root'
})
export class TodoService {

  private lastUpdated: Todo;
  private todos: Todo[] = [];
  private LOCALSTORAGE_KEY = 'TODOS';

  private draggedIndex: number;

  constructor() {
    this.todos = this.readLocalStorage();
  }

  writeLocalStorage(todos: Todo[]): void{
    localStorage.setItem(this.LOCALSTORAGE_KEY, JSON.stringify(todos));
  }

  addTodo(todo: Todo): void{
    this.todos.push(todo);
    this.writeLocalStorage(this.todos);
  }

  readLocalStorage(): Todo[]{
    const read = localStorage.getItem(this.LOCALSTORAGE_KEY);
    return read ? JSON.parse(read) : [];
  }

  getALlTodos():Todo[] {
    this.todos = this.readLocalStorage();
    return this.todos;
  }

  getTodoById(id: number): Todo{
    return this.todos[id];
  }

  getListLength(): number{
    return this.todos.length;
  }

  deleteItem(id: number): void{
    let todo = this.todos.filter(x=>x.id === id)[0];
    let index = this.todos.indexOf(todo,0);
    if(index > -1){
      this.todos.splice(index,1);
      localStorage.setItem(this.LOCALSTORAGE_KEY, JSON.stringify(this.todos));
    }
  }

  setCompleted(id: number){
    let todo = this.todos.filter(x=>x.id === id)[0];
    todo.isComplete = true;
    localStorage.setItem(this.LOCALSTORAGE_KEY, JSON.stringify(this.todos));
  }

  getAllCompleted(): Todo[]{
    const todos = this.readLocalStorage();
    return todos.filter(x=>x.isComplete);
  }

  getAllNotCompleted(): Todo[]{
    const todos = this.readLocalStorage();
    return todos.filter(x=>!x.isComplete);
  }

  getNextTodo(): Todo{
    let next: Todo;
    for(let i = 0; i < this.todos.length; i++) {
      if(!this.todos[i].isComplete){
        next = this.todos[i];
        break;
      }
    }
    return next;
  }

  createTodo(title: string, date: string, description: string, isComplete: boolean):Todo{
    const todos : Todo[] = this.readLocalStorage();
    const nextId : number = this.todos.length > 0 ? todos.length : 0;
    const currentDate = Date.now(); // zeitpunnkt der Erstellung
    //console.log(currentDate);
    const todoToCreate = new Todo(nextId, title, date, description, isComplete, currentDate);
    this.addTodo(todoToCreate);
    return todoToCreate;
  }

  updateTodo(index: number, updatedTodo: Todo){
    const modified  = Date.now(); // Zeitpunkt des Updates
    updatedTodo.modified = modified;
    this.todos[index] = updatedTodo;
    this.lastUpdated = this.todos[index];

    localStorage.setItem(this.LOCALSTORAGE_KEY, JSON.stringify(this.todos));
    //localStorage.setItem(this.UPDATE_KEY, JSON.stringify(this.lastUpdated));
  }

  getLastUpdated(): Todo {
    let currentMaxModified = 0;
    let todo: Todo;
    for (let i = 0; i < this.todos.length; i++) {
      if (this.todos[i].modified > currentMaxModified) {
        currentMaxModified = this.todos[i].modified;
        todo = this.todos[i];
      }
    }
    return todo;
  }






  allowDrop($event): void{
    $event.preventDefault();
  }

  onDragStart(index): void{
    this.draggedIndex = index;
    console.log(this.draggedIndex);
  }

  onDropOpen($event, index): void{
    $event.preventDefault();
    const todo = this.todos[this.draggedIndex];
    todo.isComplete = false;
    this.todos.splice(this.draggedIndex, 1);
    this.todos.splice(index, 0, todo);
    this.draggedIndex = -1;
    localStorage.setItem(this.LOCALSTORAGE_KEY, JSON.stringify(this.todos));
  }

  onDropDone($event, index): void{
    $event.preventDefault();
    const todo = this.todos[this.draggedIndex];
    todo.isComplete = true;
    this.todos.splice(this.draggedIndex, 1);
    this.todos.splice(index, 0, todo);
    this.draggedIndex = -1;
    localStorage.setItem(this.LOCALSTORAGE_KEY, JSON.stringify(this.todos));
  }
}
