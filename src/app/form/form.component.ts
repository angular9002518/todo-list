import {Component, OnInit} from '@angular/core';
import {Todo} from "../../models/todo.model";
import {NgForm} from "@angular/forms";
import {TodoService} from "../todo.service";

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent {

  todosArray: Todo[] = [];
  todos = new Todo(0,'','','',false);

  constructor(private todoService : TodoService) {
    //console.log('todos array class form', this.todosArray);
  }

  onSubmit(form: NgForm){
    let todo = this.todoService.createTodo(this.todos.title, this.todos.date, this.todos.description, this.todos.isComplete);
    this.todosArray.push(todo);
    //console.log('todos array class form', this.todosArray)
    form.resetForm();
  }

  setDate(id){
    let dat = document.querySelector(id);
    let hoy = Date.now();
    /*
    let d = hoy.getDate();
    let day: string;
    let m: number = hoy.getMonth()+1;
    let month: string;
    let y: number = hoy.getFullYear();
    let data: string;

    if(d < 10){
      day = '0' + d;
    }
    if(m < 10){
      month = '0' + m;
    }
    data = y+'/'+month+'/'+day;
    console.log(data);
    dat.value = data;
    * */
  }
}
