import {Component, Inject, OnInit} from '@angular/core';
import {Todo} from "../../models/todo.model";
import {TodoService} from "../todo.service";
import {NgForm} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-edit-form',
  templateUrl: './edit-form.component.html',
  styleUrls: ['./edit-form.component.css']
})
export class EditFormComponent {

  constructor(public dialogRef: MatDialogRef<EditFormComponent>,
              @Inject(MAT_DIALOG_DATA) public todo: Todo) {
  }

  close(){
    this.dialogRef.close();
  }

  onFormSubmit(form: NgForm){
    const updatedTodo = {
      ...this.todo,
      ...form.value,
    }
    this.dialogRef.close(updatedTodo);
  }

}
