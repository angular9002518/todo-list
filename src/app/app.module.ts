import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";

import {MatNativeDateModule} from '@angular/material/core';
import {HttpClientModule} from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {FormsModule , ReactiveFormsModule} from "@angular/forms";
import { FormComponent } from './form/form.component';
import { ListComponent } from './list/list.component';
import { HomeComponent } from './home/home.component';
import {CommonModule} from "@angular/common";
import {MatPaginatorModule} from "@angular/material/paginator";
import {MatCheckboxModule} from "@angular/material/checkbox";
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import {NgxPaginationModule} from "ngx-pagination";
import { EditFormComponent } from './edit-form/edit-form.component';
import {MatDialogModule} from "@angular/material/dialog";
import {DragDropModule} from "@angular/cdk/drag-drop";

@NgModule({
  declarations: [
    AppComponent,
    FormComponent,
    ListComponent,
    HomeComponent,
    HeaderComponent,
    FooterComponent,
    EditFormComponent,

  ],
  imports: [
    BrowserAnimationsModule,
    HttpClientModule,
    MatNativeDateModule,
    CommonModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MatPaginatorModule,
    MatCheckboxModule,
    NgxPaginationModule,
    MatDialogModule,
    DragDropModule,
  ],
  providers: [
    {provide: MatPaginatorModule, useClass:ListComponent}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
