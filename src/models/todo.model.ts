import {Guid} from "guid-typescript";

export class Todo{
  public id: number;
  public title: string;
  public date: string;
  public description: string;
  public isComplete: boolean;
  public modified: number;

  constructor(
    id: number,
    title: string,
    date: string,
    description: string,
    isComplete: boolean,
    modified?: number,
  ) {
    this.id = id;
    this.title = title;
    this.date = date;
    this.description = description;
    this.isComplete = isComplete;
    this.modified = modified ? modified : 0;
  }

}
